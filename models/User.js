var db = require('../lib/db');

// Define User Schema
var UserSchema = new db.Schema({
    linkedinID: {type: String, unique: true},
    firstName: {type: String},
    lastName: {type: String},
    industry: {type: String},
    location: {type: String},
    pictureUrl: {type: String},
    linkedInEmail: {type: String},
    linkedinURL: {type: String},
    positions: [ { company: { compID: Number, industry: String, name: String, size: String, type: String }, employmentInstanceID: Number, title: String, isCurrent: Boolean, endDate: { month: Number, year: Number }, startDate: { month: Number, year: Number } } ],
    educations: [{eduID: Number, degree: String, fieldOfStudy: String, schoolName: String, startDate: Number, endDate: Number}]
});

var User = db.mongoose.model('Student', UserSchema);

// Exports
module.exports.addUser = addUser;
module.exports.searchQuery = searchQuery;



function addUser(userInfo, callback) {
	var instance = new User();
	instance.linkedinID = userInfo.linkedinID;
	instance.firstName = userInfo.firstName;
	instance.lastName = userInfo.lastName;
	instance.industry = userInfo.industry;
	instance.location = userInfo.location;
	instance.pictureUrl = userInfo.pictureUrl;
	instance.emailAddress = userInfo.emailAddress;
	instance.publicProfileUrl = userInfo.publicProfileUrl;
	

	instance.save(function(err) {
		if(err) {
			callback(err);
		} else {
			callback(null, instance);
		}
	});
} // ***END addUser() Method

function searchQuery(query, callback) {
	User.find({"firstName": new RegExp(query, 'i')}, function(err, items) {
		if(err) {
			throw err;
		} else {
			callback(null, items);
		}
	})
} // ***END searchQuery Method