﻿define(['plugins/router', 'plugins/http', 'durandal/app', 'knockout'], function (router, http, app, ko) {
	var defaultRoutes = [
                { route: '', title:'Home', moduleId: 'viewmodels/home', nav: true },
                { route: 'flickr', title:'More Info', moduleId: 'viewmodels/flickr', nav: true }
            ];
    var authenticatedRoutes = [
    			{ route: '#', title:'Home', moduleId: 'viewmodels/home', nav: false},
    			{ route: '', title:'Home', moduleId: 'viewmodels/home', nav: false},
                { route: 'dashboard', title:'Dashboard', moduleId: 'viewmodels/dashboard', nav: true },
                { route: 'flickr', title:'Search Database', moduleId: 'viewmodels/flickr', nav: true }
            ];
            
	var rebuildRouter = function(routeArray){
	    router.deactivate();
	    router.reset();
	    router.routes = [];
	    router.map(routeArray).buildNavigationModel();
	    return router.activate();
	};     

    return {
    	router: router,
        userAuthenticated: ko.observable(),
        activate: function () {
            //the router's activator calls this function and waits for it to complete before proceding
            var that = this;
            http.get('/validate_authentication').then(function(response) {
            	
                that.userAuthenticated(response.userAuthenticated);
                if(response.userAuthenticated === true) {
	                rebuildRouter(authenticatedRoutes);
	                router.navigate('#/dashboard');
                } else {
	                rebuildRouter(defaultRoutes);
                }
            });
        }
    };    
});

