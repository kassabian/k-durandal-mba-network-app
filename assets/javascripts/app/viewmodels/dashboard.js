define(['plugins/router', 'plugins/http', 'durandal/app', 'knockout'], function (router, http, app, ko) {
	
	var searchData = [];
    var searchQuery = ko.observable("");
    
    return {
    	router: router,
        searchQuery: searchQuery,
        resultSet: ko.observableArray([]),
        activate: function () {
            //the router's activator calls this function and waits for it to complete before proceding
            var that = this;
            http.get('/validate_authentication').then(function(response) {
            	
                if(response.userAuthenticated === true) {
	                
                } else {
	                router.navigate('');
                }
            });
        },
        executeSearch: function() {
        	var formData = {
	        	q: searchQuery
        	};
        	var that = this;
			$.post("/search", formData, function(returnedData) {
			    // This callback is executed if the post was successful     
			    that.resultSet(returnedData.resultSet);
			});
        }
    };    
});

