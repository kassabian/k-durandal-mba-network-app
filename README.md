# Alumni Tracker LinkedIn app
## Built for FSU MBA Association

I built this alpha app to allow current and former MBA students to better connect and interact with each other outside of LinkedIn. The purpose of this app was to create a private professional social network where MBA students would be able to look for opportunites upon graduation from help provided by former MBA's.


Durandal Mimosa Skeleton
========================

This repository contains the Durandal skeleton for the Mimosa browser development toolkit.

1. Use `make start` to start up an asset server and begin coding immediately.
2. Use `make build-opt` to create your optimized application. The deployable source will be output to a 'dist' folder.
3. See the [Mimosa](http://mimosa.io/) project for details on how to customize the solution and use other features.
