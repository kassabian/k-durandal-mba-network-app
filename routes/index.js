var index = function(config) {
  cachebust = ''
  if (process.env.NODE_ENV !== "production") {
    cachebust = "?b=" + (new Date()).getTime()
  }

  var options = {
    reload: config.liveReload.enabled,
    optimize: config.isOptimize != null ? config.isOptimize : false,
    cachebust: cachebust
  };

  return function(req, res) {
    res.render("index", options);
  };
};

// Validate Authentication Method
var validateAuth = function(req, res) {
	var authenticated;
	
	if(req.session.userAuthenticated) { authenticated = true; } else { authenticated = false; }
	res.json({ userAuthenticated: authenticated});
};

var searchQuery = function(req, res) {	
	console.log(req);
};



exports.index = index;
exports.validateAuth = validateAuth;
exports.searchQuery = searchQuery;