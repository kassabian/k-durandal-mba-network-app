var express = require('express'),
    routes = require('./routes'),
    engines = require('consolidate');
var http = require('http')
  , passport = require('passport')
  , util = require('util')
  , LinkedInStrategy = require('passport-linkedin').Strategy;
var User = require('./models/User.js');
  

exports.startServer = function(config, callback) {

var port = process.env.PORT || config.server.port;
var app = express();
var server = app.listen(port, function() {
	console.log("Express server listening on port %d in %s mode", server.address().port, app.settings.env);
});

app.configure(function() {
	app.set('port', port);
	app.set('views', config.server.views.path);
	app.engine(config.server.views.extension, engines[config.server.views.compileWith]);
	app.set('view engine', config.server.views.extension);
	app.use(express.favicon());
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.cookieParser());
	app.use(express.session({ secret:'keyboard cat'}));
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(express.compress());
	app.use(config.server.base, app.router);
	app.use(express.static(config.watch.compiledDir));
});

app.configure('development', function() {
	app.use(express.errorHandler());
});


var LINKEDIN_API_KEY = "75uht5e4tocivc";
var LINKEDIN_SECRET_KEY = "UW87yEYd4qLN5StP";

passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(obj, done) {
	done(null, obj);
});


passport.use(new LinkedInStrategy({
    consumerKey: LINKEDIN_API_KEY,
    consumerSecret: LINKEDIN_SECRET_KEY,
    callbackURL: "http://localhost:3000/auth/linkedin/callback",
    profileFields: ['id', 'first-name', 'last-name', 'email-address', 'headline', 'industry', 'picture-url', 'public-profile-url', 'location', 'positions', 'educations', 'three-current-positions'],
    passReqToCallback: true
  },
  function(req, token, tokenSecret, profile, done) {
    // asynchronous verification, for effect...
    process.nextTick(function () {
      // To keep the example simple, the user's LinkedIn profile is returned to
      // represent the logged-in user.  In a typical application, you would want
      // to associate the LinkedIn account with a user record in your database,
      // and return that user instead.
      
		var d = eval("(" + profile._raw + ')')
/*		console.log(d);
		d.positions.values.forEach( function(i) {
			console.log(i);
		});*/ 
		
		var userInfo = {};
		userInfo.linkedinID = d.id;
		userInfo.firstName = d.firstName;
		userInfo.lastName = d.lastName;
		userInfo.industry = d.industry;
		userInfo.location = d.location.name;
		userInfo.pictureUrl = d.pictureUrl;
		userInfo.emailAddress = d.emailAddress;
		userInfo.publicProfileUrl = d.publicProfileUrl;
		
		// Register User into DB
		User.addUser(userInfo, function(err, instance) {
			req.session.userAuthenticated = true;
			req.session.linkedinID = d.id;
			return done(null, profile);
		});
    });
  }
));


/******************************************************************************************
** Passport LinkedIn Routes
******************************************************************************************/
app.get('/auth/linkedin', passport.authenticate('linkedin', { scope: ['r_fullprofile', 'r_emailaddress'] }));
app.get('/auth/linkedin/callback', passport.authenticate('linkedin', { failureRedirect: '/login' }),function(req, res) {
    // Successful authentication, register user and redirect to index.
    res.redirect('/');
});


/******************************************************************************************
** Begin App Routes
******************************************************************************************/
app.get('/', routes.index(config));
app.get('/validate_authentication', routes.validateAuth);

app.post('/search', function(req, res) {	
	User.searchQuery(req.body.q, function(err, items) {
		res.json({status: 'success', resultSet: items});
	});
});




callback(server);
};